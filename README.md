# AWS RDS Terraform module

Terraform module which creates RDS resources on AWS.

These types of resources are supported:

* DB instance (MySQL, Postgres, SQL Server, Oracle)
* DB Option Group (will create a new one or you may use an existing)
* DB Parameter Group
* DNS Record in Route53 for the DB endpoint

## Usage


```hcl
module "rds_instance" {
    source                      = "git::https://https://bitbucket.org/v44_crew/terraform-rds.git?ref=tags/v1.0.0"
    host_name                   = "db"
    identifier                  = "demo-db"
    vpc_security_group_ids      = ["sg-xxxxxxxx"]
    database_name               = "wordpress"
    database_user               = "admin"
    database_password           = "${random_password.password.result}"
    database_port               = 3306
    multi_az                    = "true"
    storage_type                = "gp2"
    allocated_storage           = "100"
    storage_encrypted           = "true"
    engine                      = "mysql"
    engine_version              = "5.7.17"
    major_engine_version        = "5.7"
    instance_class              = "db.t2.medium"
    db_parameter_group          = "mysql5.6"
    parameter_group_name        = "mysql-5-6"
    option_group_name           = "mysql-options"
    publicly_accessible         = "false"
    subnet_ids                  = ["sb-xxxxxxxxx", "sb-xxxxxxxxx"]
    vpc_id                      = "vpc-xxxxxxxx"
    snapshot_identifier         = "rds:production-2015-06-26-06-05"
    auto_minor_version_upgrade  = "true"
    allow_major_version_upgrade = "false"
    apply_immediately           = "false"
    maintenance_window          = "Mon:03:00-Mon:04:00"
    skip_final_snapshot         = "false"
    copy_tags_to_snapshot       = "true"
    backup_retention_period     = 7
    backup_window               = "22:00-03:00"
```

Please note that the `ref` argument in the `source` URL should always be set to the latest version value. You can find the latest version by checking for the most recent tag in the repository. 

## Inputs

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allocated_storage | The allocated storage in GBs | string | - | yes |
| allow_major_version_upgrade | Allow major version upgrade | string | `false` | no |
| apply_immediately | Specifies whether any database modifications are applied immediately, or during the next maintenance window | string | `false` | no |
| associate_security_group_ids | The IDs of the existing security groups to associate with the DB instance | list | `<list>` | no |
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| auto_minor_version_upgrade | Allow automated minor version upgrade (e.g. from Postgres 9.5.3 to Postgres 9.5.4) | string | `true` | no |
| backup_retention_period | Backup retention period in days. Must be > 0 to enable backups | string | `0` | no |
| backup_window | When AWS can perform DB snapshots, can't overlap with maintenance window | string | `22:00-03:00` | no |
| copy_tags_to_snapshot | Copy tags from DB to a snapshot | string | `true` | no |
| database_name | The name of the database to create when the DB instance is created | string | - | yes |
| database_port | Database port (_e.g._ `3306` for `MySQL`). Used in the DB Security Group to allow access to the DB instance from the provided `security_group_ids` | string | - | yes |
| db_options | A list of DB options to apply with an option group.  Depends on DB engine | list | `<list>` | no |
| db_parameter | A list of DB parameters to apply. Note that parameters may differ from a DB family to another | list | `<list>` | no |
| db_parameter_group | Parameter group, depends on DB engine used | string | - | no |
| db_subnet_group_name | DB Subnet Group Name to be used| string | - | yes |
| deletion_protection | Set to true to enable deletion protection on the RDS instance | string | `false` | no |
| enabled | Set to false to prevent the module from creating any resources | string | `true` | no |
| engine | Database engine type | string | - | yes |
| engine_version | Database engine version, depends on engine type | string | - | yes |
| final_snapshot_identifier | Final snapshot identifier e.g.: some-db-final-snapshot-2015-06-26-06-05 | string | `` | no |
| host_name | The DB host name created in Route53 | string | `db` | no |
| identifier | The unique identifier of the DB isntance | string | `` | yes |
| instance_class | Class of RDS instance | string | - | yes |
| iops | The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'. Default is 0 if rds storage type is not 'io1' | string | `0` | no |
| kms_key_arn | The ARN of the existing KMS key to encrypt storage. | string | `` | no |
| license_model | License model for this DB.  Optional, but required for some DB Engines. Valid values: license-included | bring-your-own-license | general-public-license | string | `` | no |
| maintenance_window | The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi' UTC | string | `Mon:03:00-Mon:04:00` | no |
| major_engine_version | Database MAJOR engine version, depends on engine type | string | `` | no |
| multi_az | Set to true if multi AZ deployment must be supported | string | `false` | no |
| option_group_name | Name of the DB option group to associate | string | `` | no |
| parameter_group_name | Name of the DB parameter group to associate | string | `` | no |
| password | Password for the master DB user | string | `` | yes |
| publicly_accessible | Determines if database can be publicly available (NOT recommended) | string | `false` | no |
| security_group_ids | The IDs of the security groups from which to allow `ingress` traffic to the DB instance | list | `<list>` | no |
| skip_final_snapshot | If true (default), no snapshot will be made before deleting DB | string | `true` | no |
| snapshot_identifier | Snapshot identifier e.g: rds:production-2015-06-26-06-05. If specified, the module create cluster from the snapshot | string | `` | no |
| storage_encrypted | (Optional) Specifies whether the DB instance is encrypted. The default is false if not specified. | string | `false` | no |
| storage_type | One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). | string | `standard` | no |
| subnet_ids | List of subnets for the DB | list | - | no |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |
| username | Username for the master DB user | string | `` | yes |
| vpc_id | VPC ID the DB instance will be created in | string | - | yes |
| vpc_security_group_ids | Security gorups that will be attached to the DB | list | [] | yes |

## Outputs

| Name | Description |
|------|-------------|
| instance_address | Address of the instance |
| instance_endpoint | DNS Endpoint of the instance |
| instance_id | ID of the instance |
| parameter_group_id | ID of the Parameter Group |
| db_option_group_name | ID of the DB Option Group |
| db_subnet_group_name | ID of the DB Subnet Group |
