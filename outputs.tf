output "instance_id" {
  value       = "${join("", aws_db_instance.default.*.id)}"
  description = "ID of the instance"
}

output "instance_address" {
  value       = "${join("", aws_db_instance.default.*.address)}"
  description = "Address of the instance"
}

output "instance_endpoint" {
  value       = "${join("", aws_db_instance.default.*.endpoint)}"
  description = "DNS Endpoint of the instance"
}

output "parameter_group_name" {
  value       = "${aws_db_instance.default.*.parameter_group_name}"
  description = "ID of the Parameter Group"
}

output "option_group_name" {
  value       = "${aws_db_instance.default.*.option_group_name}"
  description = "ID of the Option Group"
}

output "db_subnet_group_name" {
  value       = "${aws_db_instance.default.*.db_subnet_group_name }"
  description = "ID of the Parameter Group"
}


